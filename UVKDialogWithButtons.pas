unit UVKDialogWithButtons;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Objects,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.Layouts, FMX.Effects,
  FMX.TextLayout, UVKDialogFrame,
  System.Generics.Collections;

type
  TVKDialogWithButtons = class(TVKDialogFrame)
    sb1: TVertScrollBox;
    rectButtons: TRectangle;
    layButtons: TGridPanelLayout;
  private
  protected
    procedure BtnClick(Sender: TObject);
    procedure Resized; override;
    constructor Create(const AStartObj:TFmxObject; const ATitle:string; const AButtons: TArray<string>;
                    const AModalResults: TArray<TModalResult>; const AResultEvent: TInputCloseDialogEvent); overload; virtual;
    constructor Create(const AStartObj:TFmxObject; const ATitle:string; const AButtons: TArray<string>;
                    const AModalResults: TArray<TModalResult>; const AResultProc: TInputCloseDialogProc); overload; virtual;
    constructor Create(const AStartObj:TFmxObject; const ATitle:string); overload; virtual;
  public

    class procedure ShowDialog(AStartObj: TFmxObject;
      AMessage:string;
      AButtons: TArray<string>;
      AModalResults: TArray<TModalResult>;
      AResultEvent: TInputCloseDialogEvent ); overload;
    class procedure ShowDialog(AStartObj: TFmxObject;
      AMessage:string;
      AButtons: TArray<string>;
      AModalResults: TArray<TModalResult>;
      AResultProc: TInputCloseDialogProc ); overload;

    //     simply show the Message and OK button
    class procedure ShowDialog(AStartObj: TFmxObject; AMessage:string); overload;
  end;


implementation


{$R *.fmx}

// show dialog with result handler as anonymous function or as method
class procedure TVKDialogWithButtons.ShowDialog(
    AStartObj: TFmxObject;
    AMessage:string;
    AButtons: TArray<string>;
    AModalResults: TArray<TModalResult>;
    AResultEvent: TInputCloseDialogEvent );
begin
  FSelfRef := TVKDialogWithButtons.Create(AStartObj, AMessage, AButtons, AModalResults, AResultEvent);
end;

class procedure TVKDialogWithButtons.ShowDialog(
    AStartObj: TFmxObject;
    AMessage:string;
    AButtons: TArray<string>;
    AModalResults: TArray<TModalResult>;
    AResultProc: TInputCloseDialogProc );
begin
  FSelfRef := TVKDialogWithButtons.Create(AStartObj, AMessage, AButtons, AModalResults, AResultProc);
end;


//     simply show the Message and OK button
class procedure TVKDialogWithButtons.ShowDialog(AStartObj: TFmxObject; AMessage:string);
begin
  FSelfRef := TVKDialogWithButtons.Create(AStartObj, AMessage, ['OK'], [mrOK], NIL);
end;

constructor TVKDialogWithButtons.Create(const AStartObj: TFmxObject;
  const ATitle: string; const AButtons: TArray<string>;
  const AModalResults: TArray<TModalResult>;
  const AResultProc: TInputCloseDialogProc);
begin
  Create(AStartObj, ATitle, AButtons, AModalResults, NIL);
  FCloseProc := AResultProc;
end;

constructor TVKDialogWithButtons.Create(const AStartObj:TFmxObject; const ATitle:string; const AButtons: TArray<string>;
                                    const AModalResults: TArray<TModalResult>; const AResultEvent: TInputCloseDialogEvent);
var
  i: integer;
  b: TSpeedButton;
  item: TGridPanelLayout.TControlItem;
  index: integer;
begin
  inherited Create(AStartObj, ATitle, AResultEvent);

  if Length(AButtons)<>Length(AModalResults) then
    raise Exception.Create('���������� ������ �� ������������� ���������� ModalResults');

  if Length(AButtons)>2 then
  for i := 0 to round((length(AButtons)-2)/2+0.5)-1 do
    layButtons.RowCollection.Add;
  rectButtons.Height := layButtons.RowCollection.Count * (44+2)+2;

  for i := 0 to High(AButtons) do
  begin
    b:=TSpeedButton.Create(nil);
    b.Parent := layButtons;
    b.Align := TAlignLayout.Client;
    b.Text := AButtons[i];
    b.ModalResult := AModalResults[i];
    b.OnClick := BtnClick;
    b.Margins.Left := 5;
    b.Margins.Top := 5;
    b.Margins.Right := 5;
    b.Margins.Bottom := 5;

    if (i=High(AButtons)) and (i mod 2 =0) then
    begin
      index := layButtons.ControlCollection.IndexOf(b);
      item := layButtons.ControlCollection[index];
      item.ColumnSpan := 2;
    end;

  end;

  Resized;
end;


constructor TVKDialogWithButtons.Create(const AStartObj: TFmxObject;
  const ATitle: string);
begin
  Create(AStartObj, ATitle, ['OK'], [mrOK], NIL);
end;

procedure TVKDialogWithButtons.Resized;
var
  r: TRect;
  h,h1: single;
begin
  r := TRect.Create(0,0, Round(rectBk.Width),Round(rectBk.Height));
  r.Inflate(-r.width div 8, -r.height div 4);
  rectContainer.SetBounds(r.Left, r.Top, r.Width, r.Height);
end;

procedure TVKDialogWithButtons.BtnClick(Sender:TObject);
var b:TSpeedButton absolute Sender;
begin
  ModalResult := b.ModalResult;
  AsyncFree;
end;



end.
