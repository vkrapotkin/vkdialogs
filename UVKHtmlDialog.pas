unit UVKHtmlDialog;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Objects,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.Layouts, FMX.Effects,
  FMX.TextLayout, UVKDialogWithButtons,
  System.Generics.Collections, FMX.WebBrowser, UVKDialogFrame;

type
  TVKHtmlDialog = class(TVKDialogWithButtons)
    wb1: TWebBrowser;
  protected
    constructor Create(const AStartObj: TFmxObject; const AHtml: string); override;

  public
    class procedure ShowDialog(AStartObj: TFmxObject;
      AHTML:string); overload;
    destructor Destroy; override;
  end;


implementation
{$R *.fmx}


constructor TVKHtmlDialog.Create(const AStartObj:TFmxObject; const AHtml:string);
begin
  inherited Create(AStartObj, '');
  wb1.LoadFromStrings(AHtml, '');
  Resized;
end;


destructor TVKHtmlDialog.Destroy;
begin
  wb1.Visible := False;
  inherited;
end;

class procedure TVKHtmlDialog.ShowDialog(AStartObj: TFmxObject;
  AHTML: string);
begin
  FSelfRef := TVKHtmlDialog.Create(AStartObj, AHTML);
end;

end.
