unit UVKMessageDialog;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Objects,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.Layouts, FMX.Effects,
  FMX.TextLayout, UVKDialogFrame, UVKDialogWithButtons,
  System.Generics.Collections;

type
  TVKMessageDialog = class(TVKDialogWithButtons)
    txtMessage: TText;
  private
  protected
    constructor Create(const AStartObj:TFmxObject;
      const ATitle,AMessage:string;
      const AButtons: TArray<string>;
      const AModalResults: TArray<TModalResult>;
      const AResultEvent: TInputCloseDialogEvent); overload; virtual;
    constructor Create(const AStartObj:TFmxObject;
      const ATitle,AMessage:string;
      const AButtons: TArray<string>;
      const AModalResults: TArray<TModalResult>;
      const AResultProc: TInputCloseDialogProc); overload; virtual;
  public
    procedure Resized; override;

    class procedure ShowDialog(AStartObj: TFmxObject;
      AMessage:string;
      AButtons: TArray<string>;
      AModalResults: TArray<TModalResult>;
      AResultEvent: TInputCloseDialogEvent ); overload;
    class procedure ShowDialog(AStartObj: TFmxObject;
      AMessage:string;
      AButtons: TArray<string>;
      AModalResults: TArray<TModalResult>;
      AResultProc: TInputCloseDialogProc ); overload;
    class procedure ShowDialog(AStartObj: TFmxObject;
      AMessage:string); overload;
  end;


implementation


{$R *.fmx}

// show dialog with result handler as anonymous function or as method
class procedure TVKMessageDialog.ShowDialog(
    AStartObj: TFmxObject;
    AMessage:string;
    AButtons: TArray<string>;
    AModalResults: TArray<TModalResult>;
    AResultEvent: TInputCloseDialogEvent );
begin
  FSelfRef := TVKMessageDialog.Create(AStartObj, '', AMessage, AButtons, AModalResults, AResultEvent);
end;

class procedure TVKMessageDialog.ShowDialog(
    AStartObj: TFmxObject;
    AMessage:string;
    AButtons: TArray<string>;
    AModalResults: TArray<TModalResult>;
    AResultProc: TInputCloseDialogProc );
begin
  FSelfRef := TVKMessageDialog.Create(AStartObj, '', AMessage, AButtons, AModalResults, AResultProc);
end;

class procedure TVKMessageDialog.ShowDialog(
    AStartObj: TFmxObject;
    AMessage:string);
begin
  FSelfRef := TVKMessageDialog.Create(AStartObj,  '', AMessage, ['OK'], [mrOK], NIL);
end;

//class procedure TVKMessageDialog.ShowDialog(const AStartObj:TFmxObject; const AMessage:string);
//begin
//  f := TVKMessageDialog.Create(AStartObj, AMessage, ['OK'],[mrOK],
//  procedure (const AResult:TModalResult)
//  begin
//    f.FCloseEvent := NIL;
//    f.FCloseProc := NIL;
//   free �� ����� ������ ��� �� ��������� ������ � AsyncFree !
//  end);
//end;



//class procedure TVKMessageDialog.ShowDialog(const AStartObj:TFmxObject; const AMessage:string;
//                    const AButtons: TArray<string>;
//                    const AModalResults: TArray<TModalResult>;
//                    AResultEvent: TInputCloseDialogEvent);
//begin
//  FSelfRef := TVKMessageDialog.Create(AStartObj, AMessage, AButtons, AModalResults, nil);
//end;

constructor TVKMessageDialog.Create(const AStartObj: TFmxObject;
  const ATitle, AMessage:string;
  const AButtons: TArray<string>;
  const AModalResults: TArray<TModalResult>;
  const AResultProc: TInputCloseDialogProc);
begin
  inherited Create(AStartObj, ATitle, AButtons, AModalResults, AResultProc);
  txtMessage.Text := AMessage;
  Resized;
end;

constructor TVKMessageDialog.Create(const AStartObj:TFmxObject;
    const ATitle, AMessage:string;
    const AButtons: TArray<string>;
    const AModalResults: TArray<TModalResult>;
    const AResultEvent: TInputCloseDialogEvent);
begin
  inherited Create(AStartObj, ATitle, AButtons, AModalResults, AResultEvent);
  txtMessage.Text := AMessage;
  Resized;
end;


procedure TVKMessageDialog.Resized;
var
  r: TRect;
  h,h1: single;
  TitleHeight, MsgHeight: single;
begin
  r := TRect.Create(0,0, Round(rectBk.Width),Round(rectBk.Height));
  r.Inflate(-r.width div 8, -r.height div 8);
  if txtTitle.Text='' then
    TitleHeight := 0
  else
  begin
    TitleHeight := CalculateHeight(txtTitle, rectContainer.Width
                                   - txtTitle.Margins.Left - txtTitle.Margins.Right
                                   - sb1.Padding.Left - sb1.Padding.Right);
    txtTitle.Height := TitleHeight;
  end;
  if txtMessage.Text='' then
    MsgHeight := 0
  else
  begin
    MsgHeight := CalculateHeight(txtMessage, rectContainer.Width
                                   - txtMessage.Margins.Left - txtMessage.Margins.Right
                                   - sb1.Padding.Left - sb1.Padding.Right);
    txtMessage.Height := MsgHeight;
  end;
  h1 := Round( rectButtons.Height +
                txtTitle.Height +
                txtTitle.Margins.Top + txtTitle.Margins.Bottom +
                txtMessage.Height +
                txtMessage.Margins.Top + txtMessage.Margins.Bottom +
                sb1.Padding.Top + sb1.Padding.Bottom );
  if r.Height > Round(H1) then
    r.Height := Round(h1);
  rectContainer.SetBounds(r.Left, r.Top, r.Width, r.Height);
end;




end.
