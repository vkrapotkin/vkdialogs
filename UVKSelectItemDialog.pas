unit UVKSelectItemDialog;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  UVKDialogFrame, FMX.Effects, FMX.Layouts, FMX.Objects, FMX.ListBox,
  System.ImageList, FMX.ImgList;

type
  TVKSelectItemDialog = class(TVKDialogFrame)
    lst1: TListBox;
    li1: TListBoxItem;
    li2: TListBoxItem;
    li3: TListBoxItem;
    il1: TImageList;
    procedure lst1ItemClick(const Sender: TCustomListBox;
      const Item: TListBoxItem);
  private
    function GetResultIndex: integer;
  protected
    procedure Resized; override;
    constructor Create(const AStartObj:TFmxObject;
      const ATitle:string;
      const AItems:TStrings;
      const AResultEvent: TInputCloseDialogEvent);
  public
    procedure SetItems(const Items:TStrings); overload;
    property ResultIndex:integer read GetResultIndex;

    class procedure ShowDialog(AStartObj: TFmxObject;
        ATitle:string;
        AItems:TStrings;
        AResultEvent: TInputCloseDialogEvent );
  end;

var
  VKSelectItemDialog: TVKSelectItemDialog;

implementation

{$R *.fmx}

constructor TVKSelectItemDialog.Create(const AStartObj: TFmxObject;
  const ATitle: string;
  const AItems: TStrings;
  const AResultEvent: TInputCloseDialogEvent);
begin
  inherited Create(AStartObj, ATitle, AResultEvent);
  SetItems(AItems);
end;

function TVKSelectItemDialog.GetResultIndex: integer;
begin
  result := lst1.ItemIndex;
end;

procedure TVKSelectItemDialog.lst1ItemClick(const Sender: TCustomListBox;
  const Item: TListBoxItem);
begin
  item.ImageIndex := 1;
  ModalResult := mrOk;
  AsyncFree;
end;

procedure TVKSelectItemDialog.Resized;
var
  r: TRect;
begin
  r := TRect.Create(0,0, Round(rectBk.Width),Round(rectBk.Height));
  r.Inflate(-r.width div 20, -r.height div 20);
  rectContainer.SetBounds(r.Left, r.Top, r.Width, r.Height);
end;

procedure TVKSelectItemDialog.SetItems(const Items: TStrings);
var
  I: Integer;
  li:TListBoxItem;
begin
  lst1.Items.Clear;
  for I := 0 to Items.Count - 1 do
  begin
    li := TListBoxItem.Create(lst1);
    li.Text := Items[i];
    li.StyledSettings := li.StyledSettings - [TStyledSetting.Other];
    li.WordWrap := True;
    li.VertTextAlign := TTextAlign.Leading;
    li.ImageIndex := 0;
    lst1.AddObject(li);
  end;
end;

class procedure TVKSelectItemDialog.ShowDialog(
  AStartObj: TFmxObject;
  ATitle: string;
  AItems: TStrings;
  AResultEvent: TInputCloseDialogEvent);
begin
  FSelfRef := TVKSelectItemDialog.Create(AStartObj, ATitle, AItems, AResultEvent);
end;

end.
